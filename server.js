var express = require('express');
var cookieParser = require('cookie-parser');
var path = require('path');
var webpack = require('webpack');
var app = express();
var cors = require ('cors');

var isDevelopment = (process.env.NODE_ENV !== 'production');
var static_path = path.join(__dirname, 'public');

var _ = require('lodash');


var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.use(cors());
app.use(cookieParser());

var pg = require('pg');

if(!isDevelopment) {
  pg.defaults.ssl = true;
}


var pgp = require('pg-promise')({});



var connectionString = process.env.DATABASE_URL || 'postgress://postgres:benoit@localhost:5432/buzzword';

function getData(user, req, res) {
  var db = pgp(connectionString);

  var newUserWords;

  db.task( (task) => {
      return  task.manyOrNone(" SELECT buzzwords.word, buzzwords.id, user_word.found, user_word.confirmed, user_word.col_index, user_word.row_index FROM user_word "
                      + " JOIN buzzwords ON user_word.word_id = buzzwords.id "
                      + " WHERE user_word.user_id = $1 ", [user.id])
                  .then( (results) => {
                    if (results.length > 0) {
                      console.log(' this is enough ', results)
                      newUserWords = results;
                      return results;
                    }

                    console.log(' we need more ')
                    return task .many("SELECT * FROM buzzwords")
                                .then( (results) => {
                                  newUserWords =  _.reduce(_.shuffle(results), (acc, word, index) => {
                                    acc[word.id] = {
                                      col_index: (index % 3),
                                      row_index: Math.floor(index / 3),
                                      word: word.word,
                                      id: word.id,
                                      found: word.found,
                                      confirmed: false,
                                    } 
                                    return acc;
                                  } , {});

                                  console.log("reduced : ", newUserWords);

                                  var template = "INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ($1, $2, $3, $4, $5) returning *;";
                                  return db.tx( tx => {
                                    console.log('TX ? ');
                                    return tx.batch(
                                      _.map(newUserWords, (word) => {
                                        console.log('  > one insert ',  word.id, user.id, word.col_index, word.row_index, word.confirmed);
                                        return () => { return tx.one(template, [word.id, user.id, word.col_index, word.row_index, word.confirmed]) };
                                      })
                                    )
                                  })
                                  .then((r)=> {
                                      console.log('rr ',r)
                                      return r;
                                  })
                                  .catch(error => {
                                    console.error('ee ',error);

                                  })
                                })
                  });
  })

  .then(() => {
    console.log(" @ " , newUserWords)
    res.write(JSON.stringify(newUserWords));
    res.end();  
  })
  .catch(error => {
    console.error(error);
    res.status(500).write('oops : '+ error );
    res.end();  
  });
}

function login(name, req, res){
  var db = pgp(connectionString);
  var user;

  console.log('login:', name)

  db.task( (task) => {
    return task.oneOrNone("SELECT * FROM users WHERE name=$1", [name])
      .then((result)=>{
        if (result) {
          return result;
        } else {
          return task.one("INSERT INTO users (name) VALUES($1) RETURNING *;", [name]);
        }
      })
    })
    .then(result => {
      res.write(JSON.stringify(result));
      res.end();  
    })
    .catch(error => {
      res.status(500).write('oops 114 : '+ error );
      res.end();  
    })

}

function markWord (word, user, req, res){
  var db = pgp(connectionString);

  var data = {};
  console.log('markWord', word, user)
  db.task( (task) => {
      return  task.one("SELECT * FROM users WHERE name=$1 and id=$2", [user.name, user.id])
                  // .then( (user) => {
                  //   console.log(' :::: ', user)
                  //   data['user'] = user;
                  //   return task.one("SELECT found FROM buzzwords WHERE id=$1", [word.id]);
                  // })
                  .then( (result) => {
                    console.log(' :::: ', result, ' || ', result.found+1)
                    return task.one("update user_word set found=now() where word_id = $1 and user_id=$2 RETURNING *", [word.id, user.id]);
                  });
    })
    .then(result => {
      console.log(result)
      res.write(JSON.stringify(result));
      res.end();  
    })
    .catch(error => {
      res.status(500).write('oops : '+ error );
      res.end();  
    })

}


function validation (user_word, req, res){
  var db = pgp(connectionString);
  console.log('validation ', user_word)
  var data = {};

  db.task( (task) => {
      return  task.oneOrNone( "UPDATE user_word "
                      + " SET confirmed=TRUE " 
                      + " WHERE (user_id = $1) AND (word_id = $2) AND word_id IN ( select word_id " 
                      + "        FROM user_word " 
                      + "        WHERE (   ( found > CURRENT_TIMESTAMP  - INTERVAL '10 seconds') "
                      + "                    OR "
                      + "                  ( confirmed = TRUE ) "
                      + "              )   AND (user_id != $1) ) RETURNING *;" , [user_word.user_id, user_word.word_id]);
    })
    .then(result => {
      console.log('validation result : ', result)
      res.write(JSON.stringify(result));
      res.end();  
    })
    .catch(error => {
      res.status(500).write('oops : '+ error );
      res.end();  
    })
}


function victory (user, req, res){
  var db = pgp(connectionString);

  db.task( (task) => {
      return  task.oneOrNone('select * from victors where user_id = $1',  [user.id])
                  .then(result => {
                    if (result){
                      return [result];                      
                    } else {
                      return task.one( "INSERT INTO victors (user_id, victory_time) VALUES( $1, NOW() ) RETURNING *;" , [user.id]);
                    }
                  })
                  .then((result)=>{
                    return task.many("SELECT * FROM victors JOIN users ON users.id = victors.user_id ORDER BY victory_time asc");
                  })
    })

    .then(result => {
      console.log('victory result : ', result)
      res.write(JSON.stringify(result));
      res.end();  
    })
    .catch(error => {
      res.status(500).write('oops : '+ error );
      res.end();  
    })
}





app.use(express.static(static_path))
  .get('/', function (req, res) {
    res.sendFile('index.html', {
      root: static_path
    });
  });

app.options('/users') ;
app.post   ('/users',  (req, res) => {
  var name = req.body['name'];
  login(name, req, res);
})


app.options('/user_word') ;
app.post   ('/user_word',  (req, res) => {
  var user = req.body['user'];
  var word = req.body['word'];
  markWord(word, user, req, res);
})

app.put   ('/user_word/:id/validation',  (req, res) => {
  var user_word = req.body;
  validation(user_word, req, res);
})


app.get('/users/:id/words/', (req, res) => {
  getData({id:req.params.id}, req, res);
})

app.options('/victory') ;
app.post('/victory', (req, res) => {
  var user = req.body['user'];
  victory(user, req, res);
})


app.listen(process.env.PORT || 8080, function (err) {
    if (err) { console.log(err) };
    console.log('Listening at localhost:8080');
  });




if (isDevelopment) {
  var config = require('./webpack.config');
  var WebpackDevServer = require('webpack-dev-server');

  new WebpackDevServer(webpack(config), {
    publicPath: config.output.publicPath,
    hot: true,
    historyApiFallback: true,
    stats:{ colors: true}

  }).listen(3000, 'localhost', function (err, result) {
    if (err) { console.log(err) }
    console.log('Listening at localhost:3000');
  });
}
