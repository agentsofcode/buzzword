import React                                    from 'react';
import ReactDOM                                 from 'react-dom';
import {Router, hashHistory, Route, IndexRoute}             from 'react-router';
import {createStore, applyMiddleware, compose}  from 'redux';
import {Provider}                               from 'react-redux';
import {List, Map, fromJS}                      from 'immutable';
import $                                        from 'jquery';
import _                                        from 'lodash';
import thunk                                    from 'redux-thunk';


import {App}    from './components/app';
import {Game}   from './components/game';
import {Login}  from './components/login';
import {Logout} from './components/logout';

import reducers  from './reducers';



const routes =  <Route name='app' path='/' component={App}> 
                  <Route name='login' path="/login" component={Login} />   
                  <Route name='logout' path="/logout" component={Logout} />   
                  <Route name='game'  path="/game"  component={Game} />   
                </Route>;

import './css/app.scss';

let isDevelopment = (process.env.NODE_ENV !== 'production');

let store;

if (isDevelopment) {
  store = createStore(reducers, Map(), 
  compose(
      // applyMiddleware(LocalStorage),
    applyMiddleware(thunk),

    (window.devToolsExtension) ? window.devToolsExtension() : undefined
  )
);

} else {
  store = createStore(reducers, Map(), 
 // compose(
    applyMiddleware(thunk)
 // )
  );

}


store.dispatch({
  type: 'INIT',
  data: {
    words: {
      1: { text: 'benoit',     found: 0, col: 0, row: 0, id: 1},
      2: { text: 'tessella',   found: 0, col: 1, row: 0, id: 2},
      3: { text: 'kebab',      found: 0, col: 2, row: 0, id: 3},
      4: { text: 'react',      found: 0, col: 0, row: 1, id: 4},
      5: { text: 'redux',      found: 0, col: 1, row: 1, id: 5},
      6: { text: 'star wars',  found: 0, col: 2, row: 1, id: 6},
      7: { text: 'batman',     found: 0, col: 0, row: 2, id: 7},
      8: { text: 'superman',   found: 0, col: 1, row: 2, id: 8},
      9: { text: 'bacon',      found: 0, col: 2, row: 2, id: 9},
    },
    user: {},
    victories: []
  }
});


ReactDOM.render(
  <Provider store={store} >
    <Router history={hashHistory}>{routes}</Router>
  </Provider>
  ,
  document.getElementById('app'));
