import {List, Map, fromJS}  from 'immutable';

const initialState =  fromJS({words:[]})


export default function reducers (state=initialState, action){
  let _state;

  switch(action.type) {
    case 'INIT':
      return Map(fromJS(action.data));

    case 'WORDS_RECEIVED':
      let words = _.reduce(action.words, ( acc, word ) => {
        acc[word.id] = {
          col: word.col_index,
          row: word.row_index,
          text: word.word,
          id: word.id,
          found: null,
          confirmed: word.confirmed,
        }
        return acc;
      } , {} );
      return state.updateIn(['words'], '', v =>  { return Map(fromJS(   words   ) );  } ); 

    case 'LOGIN':
      return state.updateIn(['user'], '', v =>  { return Map(fromJS( { name: action.name, id: action.id }) );  } ); 

    case 'LOGOUT':
      return state.updateIn(['user'], {}, v =>  { return Map() } ); 

    case 'WORD_VALIDATED':
      // console.log('validated word', action)
      _state = state.updateIn(['words', action.user_word.word_id+'', 'confirmed' ], false, (v) =>  { return true } ); 
      // _state = state.updateIn(['words', action.word_id+'', 'found' ], 0, (v) =>  { return v + 1} ); 
      return _state;

    case 'WORD_INVALIDATED':
      // console.log('invalidated word', action)
      _state = state.updateIn(['words', action.word_id+'', 'found' ], false, (v) =>  null ); 
      return _state;

    case 'WORD_MARKED':
      // console.log('marked word', action)
      return state.updateIn(['words', action.word.word_id+'', 'found' ], 0, (v) =>  { return action.word.found} ); 
    case 'BINGO':
      // console.log('marked word', action)
      return state.updateIn(['victories'], [], (v) =>  { return  fromJS(action.victories) } ); 
  }
  return state;
}
