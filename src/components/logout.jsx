import React          from 'react';
import { connect }        from 'react-redux';
import { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';

import * as actionCreators from '../action_creators';
import Buzzword from './buzzword';

export default class _Logout extends React.Component {

  onClick(){
    this.props.actions.logout();
  }

  render() {
    let content;


    if (!this.props.user.id) {
      content = <div className='container'>
                  <div className="screen">
                    <p>
                      Good bye...
                    </p>
                  </div>
                  <div className="overlay"></div>
                </div>
    }
    else {
      content = <div className='logoutForm'>
                  <p>
                    Are you certain you want to log out?
                  </p>
                  <div className='buttonForm' onClick={this.onClick.bind(this)}>
                    <div>Yes...</div>
                  </div>
                </div>;
    }

    return  <div>
              {content}
            </div>

  }
}





const mapStateToProps = (state) => {
  return {
    user: state.getIn(['user']).toJS()
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

export const Logout = connect(
  mapStateToProps 
 ,mapDispatchToProps
)(_Logout);
