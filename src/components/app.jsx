import React                  from 'react';
import { connect }            from 'react-redux';
import { Component }          from 'react';
import { bindActionCreators } from 'redux';
import { Link }               from 'react-router';

import * as actionCreators    from '../action_creators';


import url from '../img/fish.gif' ;


export default class _App extends React.Component {

  render() {
      return  <div className="main"> 
                <div className="menu"> 
                  { this.props.user.id ? 
                      <ul>
                        <li> <div className='menuItem'><Link to="logout">logout</Link></div> </li>
                        <li> <Link to="game"> <div className='menuItem'>game</div></Link> </li>
                      </ul>
                    : <ul>
                        <li> <Link to="login"><div className='menuItem'>login</div></Link> </li>
                      </ul>
                  }
                  <div className='logoplaceholder'>
                    <div className='logo rotoshake'>
                      <img src={url} />
                    </div>
                  </div>
                </div>
                <div>

                  {this.props.children}
                </div>
            </div>;
  }
}





const mapStateToProps = (state) => {
  return {
    user: state.getIn(['user']).toJS()
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

export const App = connect(
  mapStateToProps 
 ,mapDispatchToProps
)(_App);
