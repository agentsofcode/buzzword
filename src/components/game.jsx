import React                                        from 'react';
import { connect }                                  from 'react-redux';
import { Component, PropTypes }                     from 'react';
import { bindActionCreators }                       from 'redux';

import * as actionCreators      from '../action_creators';
import Buzzword                 from './buzzword';
import VictoryList                 from './victory-list';
import classNames from 'classnames';


export default class _Game extends React.Component {

  clickBingo() {
    this.props.actions.bingo(this.props.user);
  }

  withBingo(words){
    return _.chain(words)
            .reduce((acc, word) => {
              if (word.confirmed){
                acc[ word.col     ] += 1;
                acc[ word.row + 3 ] += 1;
              }
              return acc;
            }, [0,0,0,0,0,0])
            .some( v => (v === 3) )
            .value();
  }

  render() {
    let actions = this.props.actions;
    let user    = this.props.user;

    let wordArray = _.reduce(this.props.words, (acc, word) => {
                        acc[word.row][word.col] = word;
                        return acc;
                      }, [ ['', '', ''], ['', '', ''], ['', '', ''] ] )

    let buzzwords = _.map(wordArray, (row, rowIndex) => {
      let cells = _.map(row, (word, colIndex) => {
        return <td key={ rowIndex + '' + colIndex } ><Buzzword word={word} user={user} actions={actions}/></td>
      });
      return <tr key={ rowIndex + '' } >{ cells }</tr>
    }) 


    let bingoButtonClass = 'bingo';
    if ( this.withBingo(this.props.words) ) {
      bingoButtonClass += ' enabled tada';
    } 

    if (this.props.user.id){
      if (this.props.victories && this.props.victories.length > 0) {
        return <VictoryList user={user} victories={this.props.victories}/>;
      }
      else 
      {
        return <div>
                  <table className='board'>
                    <tbody>
                      { buzzwords }
                    </tbody>
                  </table>
                  <div className={bingoButtonClass} onClick={this.clickBingo.bind(this)}> <div>BINGO</div> </div>
                </div>;
      }
    } 

    return <div> . </div>;  
  }
}





const mapStateToProps = (state) => {
  return {
    words: state.getIn(['words']).toJS(),
    user: state.getIn(['user']).toJS(),
    victories: state.getIn(['victories']).toJS(),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

export const Game = connect(
  mapStateToProps 
 ,mapDispatchToProps
)(_Game);
