import React                                        from 'react';
import { Component, PropTypes }                     from 'react';

import classNames from 'classnames';


export default class VictoryList extends React.Component {

  render() {
    let user = this.props.user;

    if (user.id && this.props.victories) {
      let victories = _.map(this.props.victories, (v, i) => {

        let victoryClasses = 'victory';

        if ( v.user_id === user.id ) {
          victoryClasses += ' ownVictory';
        }
        if ( i === 0 ) {
          victoryClasses += ' winner';
        }

        let d = new Date(v.victory_time);


        return  <li key={i} className={victoryClasses}>
                  <div className='name'>{i+1} - { v.name }</div>
                  <div className='time'>{ d.toLocaleTimeString('en-GB')}</div>                   
                </li>;
      })
      return  <div className='victoryList'>
                <div>
                  Victory List
                </div>
                <ol className='victories'>
                  {victories}
                </ol>
              </div>;
    } 

    return <div></div>;  
  }
}
