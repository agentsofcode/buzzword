import React          from 'react';
import { connect }        from 'react-redux';
import { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';

import * as actionCreators from '../action_creators';
import Buzzword from './buzzword';

export default class _Login extends React.Component {

  onClick(){
    if (this.refs.nameInput) {
      this.props.actions.login(this.refs.nameInput.value);
    }
  }

  render() {
    let content;



    if (this.props.user.id) {
      content = <div className='container'>
                  <div className="screen">
                    <p>
                                      Hello {this.props.user.name}
                    </p>
                    <p>
                      Shall we play a game?
                    </p>
                  </div>
                  <div className="overlay"></div>
                </div>
    }
    else {
      content = <div className='loginForm'>
                  <div className='inputForm'>
                    <div>Enter your name</div>
                    <input type="text" ref="nameInput"></input>
                  </div>
                  <div className='buttonForm' onClick={this.onClick.bind(this)}>
                    <div>Go!</div>
                  </div>
                </div>;
    }

    return  <div>
              {content}
            </div>


  }
}


/*<button onClick={this.onInit.bind(this)}>init</button>*/



const mapStateToProps = (state) => {
  return {
    user: state.getIn(['user']).toJS()
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

export const Login = connect(
  mapStateToProps 
 ,mapDispatchToProps
)(_Login);
