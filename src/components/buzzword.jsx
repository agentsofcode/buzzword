import React                    from 'react';
import { Component, PropTypes } from 'react';

import classNames from 'classnames';

export default class buzzword extends React.Component {
  constructor (){
    super();
  }

  onClick(){
    // console.log('click', this.props.word, this.props.user)
    this.props.actions.markWord(this.props.word, this.props.user);
  }

  render() {

    let word = this.props.word;

    let divClass = 'buzzword';
    if (word.found) {
      divClass += ' found wobble';
    }
    if (word.confirmed) {
      divClass += ' confirmed';
    }

    return  <div className={divClass} onClick={this.onClick.bind(this)}   >
              <div>{word.text}</div>
            </div>;
  }
};
