import $  from 'jquery';



let serverURL = '';
let isDevelopment = (process.env.NODE_ENV !== 'production');

if(isDevelopment){
  serverURL = "http://localhost.com:8080";
}


// ======== init =============

function _init (data) {
  return {
    type: 'INIT',
    data
  };
}

export function init(data) {
  return (dispatch, getState) => {
    dispatch(_init(data))
  }
}

// ======== login =============

function _login(user){
  return  {
    type: 'LOGIN',
    name: user.name,
    id: user.id
  }
}

function _gotWords(words) { 
  return { 
    type: 'WORDS_RECEIVED', 
    words
  }
}

export function login(name){
  let user;
  return (dispatch) => {
    return $.ajax({
      url:serverURL + '/users',
      data: JSON.stringify({name}),
      method:'POST', 
      contentType: "application/json; charset=utf-8",
    })
    .then( response => { 
      user = JSON.parse(response);
      // user.id = 1;
      return dispatch(_login(user));
    })
    .then( () => {
      console.log('  :: ' + serverURL + '/users/' + user.id + '/words/')
      return $.ajax({
        url:serverURL + '/users/' + user.id + '/words/',
        method:'GET', 
      })
    })
    .then( response => { 
      dispatch(_gotWords(JSON.parse(response)))
    });
  }
}


// ======== Bingo! =============

function _bingo(victories) { 
  return { 
    type: 'BINGO', 
    victories
  }
}

export function bingo(user){
  // let user;
  return (dispatch) => {
    return $.ajax({
      url:serverURL + '/victory',
      data: JSON.stringify({user}),
      method:'POST', 
      contentType: "application/json; charset=utf-8",
    })
    .then( response => { 
      let victories = JSON.parse(response);
      // user.id = 1;
      return dispatch(_bingo(victories));
    });
  }
}



// ======== mark word =============


function _markWord(word) { 
  return { 
    type: 'WORD_MARKED', 
    word
  }
}

function _validateWord(user_word) { 
  return { 
    type: 'WORD_VALIDATED', 
    user_word
  }
}

function _invalidateWord(word_id) { 
  return { 
    type: 'WORD_INVALIDATED', 
    word_id
  }
}


function _validate(user_word){
  return (dispatch) => {
    console.log('_validate:', user_word)
    return $.ajax({
      url:serverURL + '/user_word/' + user_word.id + '/validation',
      data: JSON.stringify(user_word),
      method:'put', 
      contentType: "application/json; charset=utf-8",
    })
    .then( (response) => {
      let result = JSON.parse(response);
      if ( result ) {
        console.log('_validate result : ', result)
        dispatch(_validateWord(result));
      } else {
        console.log('_invalidate result : ', user_word.id)
        dispatch(_invalidateWord(user_word.word_id));
      }
    })
    
  }
}

export function markWord(word, user) { 
  return (dispatch) => {
    let user_word;

    return $.ajax({
      url:serverURL + '/user_word',
      data: JSON.stringify({word, user}),
      method:'POST', 
      contentType: "application/json; charset=utf-8",
    })
    .then( response => {
      user_word = JSON.parse(response);
      return dispatch(_markWord(user_word))
    })
    .then( () => {
      setTimeout( () => {
        dispatch(_validate(user_word));
      }, 2000);
    });
  }
}





// ======== logout =============

export function logout(name){
  return {
    type: 'LOGOUT'
  }
}
