﻿-- Database: d6e03hbee8khja

DROP TABLE IF EXISTS victors;
DROP TABLE IF EXISTS user_word;
DROP TABLE IF EXISTS buzzwords;
DROP TABLE IF EXISTS users;

CREATE TABLE buzzwords
(
  word varchar(255),
  id SERIAL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE buzzwords
  OWNER TO wenynbjccgwmqu;
UPDATE buzzwords SET id = DEFAULT;
ALTER TABLE buzzwords ADD PRIMARY KEY (id);


CREATE TABLE users
(
  name varchar(255),
  id SERIAL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO wenynbjccgwmqu;
UPDATE users SET id = DEFAULT;
ALTER TABLE users ADD PRIMARY KEY (id);



CREATE TABLE user_word
(
  id SERIAL,
  word_id integer NOT NULL,
  user_id integer NOT NULL,
  col_index integer NOT NULL,
  row_index integer NOT NULL,
  found TIMESTAMP,
  confirmed boolean,
  CONSTRAINT user_fk FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT word_fk FOREIGN KEY (word_id)
      REFERENCES buzzwords (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_word
  OWNER TO wenynbjccgwmqu;
UPDATE user_word SET id = DEFAULT;
ALTER TABLE user_word ADD PRIMARY KEY (id);





CREATE TABLE victors
(
  user_id integer not null,
  victory_time TIMESTAMP,
  id SERIAL,
  CONSTRAINT victor_user_fk FOREIGN KEY (user_id)
      REFERENCES users(id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE victors
  OWNER TO wenynbjccgwmqu;
UPDATE victors SET id = DEFAULT;
ALTER TABLE victors ADD PRIMARY KEY (id);




INSERT INTO buzzwords(word) VALUES ('blue chip'        );
INSERT INTO buzzwords(word) VALUES ('tessella'         );
INSERT INTO buzzwords(word) VALUES ('altran'           );
INSERT INTO buzzwords(word) VALUES ('grow the business');
INSERT INTO buzzwords(word) VALUES ('arsenal'          );
INSERT INTO buzzwords(word) VALUES ('puppy'            );
INSERT INTO buzzwords(word) VALUES ('industry'         );
INSERT INTO buzzwords(word) VALUES ('solar orbiter'    );
INSERT INTO buzzwords(word) VALUES ('culture'          );
