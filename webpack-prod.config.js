var path = require('path');
var webpack = require('webpack');

var definePlugin = new webpack.DefinePlugin({
  'process.env':{
    'NODE_ENV': JSON.stringify('production')
  }
})

module.exports = {
  devtool: 'source-map',

  entry: [
    './src/entry'
  ],

  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js',
    publicPath: '/'
  },

  plugins: [
    // new webpack.optimize.DedupePlugin(),
    // new webpack.optimize.UglifyJsPlugin({
    //   minimize: true,
    //   compress: {
    //     warnings: true
    //   }
    // }),
    definePlugin
  ],

  resolve: {
    extensions: ['', '.js', '.jsx']
  },

  module: {
    loaders: [
      { 
        test: /\.jsx$|\.js$/,
        loaders: ['babel'],
        include: path.join(__dirname, 'src') 
      },
      { 
        test: /\.js?$/,
        loader: 'babel?stage=0',
        exclude: /node_modules/ 
      },
      { 
        test: /\.scss?$/,
        loader: 'style!css!sass',
        include: path.join(__dirname, 'src/css') 
      },
      {
        test: /\.(png|jpg|gif)$/, 
        loader: 'file?name=[path][name][hash].[ext]!img' 
      },
      { 
        test: /\.(ttf|otf)$/,
        loader: 'file?name=[path][name].[hash].[ext]',
        // include: path.join(__dirname, 'fonts'),
      },
    ]
  }
}
