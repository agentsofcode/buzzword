var path = require('path');
var webpack = require('webpack');

var definePlugin = new webpack.DefinePlugin({
  'process.env':{
    'NODE_ENV': JSON.stringify('development')
  }
})

module.exports = {
  devtool: 'eval',

  entry: [
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/only-dev-server',
    './src/entry'
  ],

  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js',
    publicPath: '/public/'
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    definePlugin
  ],

  resolve: {
    extensions: ['', '.js', '.jsx']
  },

  module: {
    loaders: [
      { 
        test: /\.jsx$|\.js$/,
        loaders: ['react-hot', 'babel'],
        include: path.join(__dirname, 'src') 
      },
      { 
        test: /\.js$/,
        loader: 'babel?stage=0',
        include: path.join(__dirname, 'src') 
      },
      { 
        test: /\.scss?$/,
        loader: 'style!css!sass',
        include: path.join(__dirname, 'css') 
      },
      { 
        test: /\.gif$/,
        loader: 'file!img',
        include: path.join(__dirname, 'img') 
      },
      { 
        test: /\.(ttf|otf)$/,
        loader: 'file',
        include: path.join(__dirname, 'fonts') 
      },
    ]
  }
}
