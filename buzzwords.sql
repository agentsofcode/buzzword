﻿DROP TABLE IF EXISTS victors;
DROP TABLE IF EXISTS user_word;
DROP TABLE IF EXISTS buzzwords;
DROP TABLE IF EXISTS users;

CREATE TABLE buzzwords
(
  word varchar(255),
  id SERIAL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE buzzwords
  OWNER TO postgres;
UPDATE buzzwords SET id = DEFAULT;
ALTER TABLE buzzwords ADD PRIMARY KEY (id);


CREATE TABLE users
(
  name varchar(255),
  id SERIAL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;
UPDATE users SET id = DEFAULT;
ALTER TABLE users ADD PRIMARY KEY (id);


CREATE TABLE user_word
(
  id SERIAL,
  word_id integer NOT NULL,
  user_id integer NOT NULL,
  col_index integer NOT NULL,
  row_index integer NOT NULL,
  found TIMESTAMP,
  confirmed boolean,

  CONSTRAINT user_fk FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT word_fk FOREIGN KEY (word_id)
      REFERENCES buzzwords (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
  
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_word
  OWNER TO postgres;
UPDATE user_word SET id = DEFAULT;
ALTER TABLE user_word ADD PRIMARY KEY (id);



CREATE TABLE victors
(
  user_id integer not null,
  victory_time TIMESTAMP,
  id SERIAL,
  CONSTRAINT victor_user_fk FOREIGN KEY (user_id)
      REFERENCES users(id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE victors
  OWNER TO postgres;
UPDATE victors SET id = DEFAULT;
ALTER TABLE victors ADD PRIMARY KEY (id);



--INSERT INTO users(name, id) VALUES ('benoit',  1);
--INSERT INTO users(name, id) VALUES ('sam',     2);
--INSERT INTO users(name, id) VALUES ('tristan', 3);
INSERT INTO users(name) VALUES ('benoit') ;
INSERT INTO users(name) VALUES ('tristan') ;
INSERT INTO users(name) VALUES ('jasper') ;

INSERT INTO buzzwords(word) VALUES ('blue chip'        );
INSERT INTO buzzwords(word) VALUES ('tessella'         );
INSERT INTO buzzwords(word) VALUES ('altran'           );
INSERT INTO buzzwords(word) VALUES ('grow the business');
INSERT INTO buzzwords(word) VALUES ('arsenal'          );
INSERT INTO buzzwords(word) VALUES ('puppy'            );
INSERT INTO buzzwords(word) VALUES ('industry'         );
INSERT INTO buzzwords(word) VALUES ('solar orbiter'    );
INSERT INTO buzzwords(word) VALUES ('culture'          );

INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 1, 1, 0, 0, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 2, 1, 0, 1, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 3, 1, 0, 2, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 4, 1, 1, 0, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 5, 1, 1, 1, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 6, 1, 1, 2, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 7, 1, 2, 0, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 8, 1, 2, 1, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 9, 1, 2, 2, false);

INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 1, 2, 0, 0, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 2, 2, 0, 1, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 3, 2, 0, 2, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 4, 2, 1, 0, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 5, 2, 1, 1, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 6, 2, 1, 2, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 7, 2, 2, 0, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 8, 2, 2, 1, false);
INSERT INTO user_word(word_id, user_id, col_index, row_index, confirmed) VALUES ( 9, 2, 2, 2, false);


